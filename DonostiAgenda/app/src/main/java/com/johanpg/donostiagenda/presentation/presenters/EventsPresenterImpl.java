package com.johanpg.donostiagenda.presentation.presenters;

import android.view.View;

import com.johanpg.donostiagenda.commons.BusProvider;
import com.johanpg.donostiagenda.domain.EventsInteractor;
import com.johanpg.donostiagenda.domain.EventsInteractorImpl;
import com.johanpg.donostiagenda.model.entities.Event;
import com.johanpg.donostiagenda.presentation.views.EventsView;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

/**
 * <h3>Implementación de {@link EventsPresenter}</h3>
 * <p/>
 * Este presentador establece una comunicación entre{@link EventsView}
 * y {@link EventsInteractorImpl}.
 *
 * @author Johan
 * @since 5/22/2015.
 */
public class EventsPresenterImpl implements EventsPresenter {

    private EventsView eventsView;
    private EventsInteractor eventsInteractor;

    /**
     * Este bus de eventos permite comunicar el interactor con el
     * presentador, y así, el presentador sabrá cuando el interacor
     * ha terminado sus tareas.
     */
    public static Bus uiBus;

    /**
     * Inicializa la vista, el interactor y el bus de eventos.
     *
     * @param eventsView
     */
    public EventsPresenterImpl(EventsView eventsView) {
        this.eventsView = eventsView;
        uiBus = BusProvider.getUIBusInstance();
        uiBus.register(this);
        eventsInteractor = new EventsInteractorImpl(uiBus);
    }

    @Override
    public void onEventClicked(View view, int position) {
        eventsView.showDetailEvent(view, position);
    }

    @Override
    public void onCategoryClicked(int category) {
        eventsView.showProgress();
        eventsInteractor.getEventsByCategory(category);
    }

    @Override
    public void onCreate() {
        if (eventsView.isEventListEmpty()) {
            eventsView.showProgress();
            eventsInteractor.getEvents();
        }
    }

    /**
     * Subscribimos este método al bus para permitir que el interactor
     * comunique al presentador que los datos están listos.
     *
     * @param events
     */
    @Subscribe
    public void onSuccess(ArrayList<Event> events) {
        eventsView.showEvents(events);
        eventsView.hideProgress(true);
    }

    /**
     * Subscribimos este método al bus para permitir que el interactor
     * comunique al presentador que ha habido un error.
     */
    @Subscribe
    public void onError(String error) {
        eventsView.hideProgress(false);
        eventsView.showErrorMessage("Ups! Intentalo de nuevo.");
    }
}
