package com.johanpg.donostiagenda.model.entities;

import java.io.Serializable;

/**
 * <h3>Entidad Evento</h3>
 * <p/>
 * Esta clase representa un evento con todas sus propiedades, además,
 * contiene los métodos para acceder a estas.
 *
 * @author Johan
 * @since 5/23/2015.
 */
public class Event implements Serializable{

    private Integer idEvent;
    private Integer idUser;
    private Integer idCategory;
    private Integer idSite;
    private Boolean state;
    private String title;
    private String summary;
    private String datetime;
    private Float duration;
    private Float language;
    private Float cost;
    private String image;
    private String video;

    /**
     * Inicializa un evento con las propiedades minimas necesarias.
     *
     * @param idEvent
     * @param idCategory
     * @param idSite
     * @param title
     * @param summary
     * @param image
     * @param datetime
     */
    public Event(Integer idEvent, Integer idCategory, Integer idSite, String title, String summary,
                 String image, String datetime) {
        this.idEvent = idEvent;
        this.idCategory = idCategory;
        this.idSite = idSite;
        this.title = title;
        this.summary = summary;
        this.image = image;
        this.datetime = datetime;
    }

    /**
     * @return The idEvent
     */
    public Integer getIdEvent() {
        return idEvent;
    }

    /**
     * @param idEvent The id_event
     */
    public void setIdEvent(Integer idEvent) {
        this.idEvent = idEvent;
    }

    /**
     * @return The idUser
     */
    public Integer getIdUser() {
        return idUser;
    }

    /**
     * @param idUser The id_user
     */
    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    /**
     * @return The idCategory
     */
    public Integer getIdCategory() {
        return idCategory;
    }

    /**
     * @param idCategory The id_category
     */
    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    /**
     * @return The idSite
     */
    public Integer getIdSite() {
        return idSite;
    }

    /**
     * @param idSite The id_site
     */
    public void setIdSite(Integer idSite) {
        this.idSite = idSite;
    }

    /**
     * @return The state
     */
    public Boolean getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(Boolean state) {
        this.state = state;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * @param summary The summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * @return The datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime The datetime
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    /**
     * @return The duration
     */
    public Float getDuration() {
        return duration;
    }

    /**
     * @param duration The duration
     */
    public void setDuration(Float duration) {
        this.duration = duration;
    }

    /**
     * @return The language
     */
    public Float getLanguage() {
        return language;
    }

    /**
     * @param language The language
     */
    public void setLanguage(Float language) {
        this.language = language;
    }

    /**
     * @return The cost
     */
    public Float getCost() {
        return cost;
    }

    /**
     * @param cost The cost
     */
    public void setCost(Float cost) {
        this.cost = cost;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The video
     */
    public String getVideo() {
        return video;
    }

    /**
     * @param video The video
     */
    public void setVideo(String video) {
        this.video = video;
    }
}