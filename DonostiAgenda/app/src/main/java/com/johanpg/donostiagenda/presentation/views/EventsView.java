package com.johanpg.donostiagenda.presentation.views;

import android.view.View;

import com.johanpg.donostiagenda.model.entities.Event;

import java.util.ArrayList;

/**
 * <h3>Vista de eventos</h3>
 * <p/>
 * Interfaz que muestra los datos del modelo de eventos y envía los
 * eventos (comandos como el onClick) al presentador.
 *
 * @author Johan
 * @since 5/22/2015.
 */
public interface EventsView {

    /**
     * Muestra la barra de progreso.
     */
    void showProgress();

    /**
     * Oculta la barra de progreso.
     *
     * @param result
     */
    void hideProgress(boolean result);

    /**
     * Muestra los eventos.
     *
     * @param events
     */
    void showEvents(ArrayList<Event> events);

    /**
     * Muestra un mensaje si ha ocurrido algún problema obteniendo
     * los datos.
     *
     * @param message
     */
    void showErrorMessage(String message);

    /**
     * Responde si la lista de eventos esta vacia o no.
     *
     * @return boolean
     */
    boolean isEventListEmpty();

    /**
     * Muestra los detalles del evento pulsado en una nueva Activity.
     */
    void showDetailEvent(View view, int position);
}
