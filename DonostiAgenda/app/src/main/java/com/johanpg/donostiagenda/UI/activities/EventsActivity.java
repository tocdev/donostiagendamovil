package com.johanpg.donostiagenda.ui.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.johanpg.donostiagenda.R;
import com.johanpg.donostiagenda.model.entities.Category;
import com.johanpg.donostiagenda.model.entities.Event;
import com.johanpg.donostiagenda.presentation.presenters.EventsPresenter;
import com.johanpg.donostiagenda.presentation.presenters.EventsPresenterImpl;
import com.johanpg.donostiagenda.presentation.views.EventsView;
import com.johanpg.donostiagenda.ui.views.adapters.EventsAdapter;
import com.johanpg.donostiagenda.ui.views.views.RecyclerInsetsDecoration;
import com.johanpg.donostiagenda.ui.views.views.RecyclerViewClickListener;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import net.steamcrafted.loadtoast.LoadToast;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class EventsActivity extends AppCompatActivity implements EventsView,
        Drawer.OnDrawerItemClickListener, RecyclerViewClickListener {

    /*
    * Inyectamos las vistas haciendo uso de la librería ButterKnife.
    * De esta forma, reducimos las lineas de código para trabajar con
    * las vistas en Android.
     */
    @InjectView(R.id.events_recycler)
    RecyclerView eventRecycler;
    @InjectView(R.id.events_toolbar)
    Toolbar toolbar;

    public Drawer drawer;
    public static SparseArray<Bitmap> photoCache;
    public static final String EXTRA_POSITION = "position";
    public static final String EXTRA_EVENT = "selected_event";
    public static final String TRANSITION_EVENT = "event";

    private LoadToast loadToast;
    private ArrayList<Event> events;
    private EventsAdapter recyclerAdapter;
    private EventsPresenter presenter;
    private static final int COLUMNS = 2;

    @Override
    public void showProgress() {
        loadToast = new LoadToast(this)
                .setTranslationY(400)
                .setText(getResources().getString(R.string.loading_events))
                .setBackgroundColor(getResources().getColor(R.color.color_primary))
                .setTextColor(getResources().getColor(R.color.color_toolbar_title))
                .setProgressColor(getResources().getColor(R.color.color_toolbar_title))
                .show();
    }

    @Override
    public void onEventClick(View view, int position) {
        presenter.onEventClicked(view, position);
    }

    @Override
    public void hideProgress(boolean result) {
        if (result) {
            loadToast.success();
        } else {
            loadToast.error();
        }
    }

    @Override
    public void showEvents(ArrayList<Event> eventList) {
        recyclerAdapter.refreshEvents(eventList);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDetailEvent(View view, int position) {
        if (view instanceof ImageView) {
            Event selectedEvent = events.get(position);
            Intent detailIntent = new Intent(EventsActivity.this, DetailActivity.class);
            detailIntent.putExtra(EXTRA_POSITION, position);
            detailIntent.putExtra(EXTRA_EVENT, selectedEvent);

            ImageView eventImage = (ImageView) view.findViewById(R.id.item_event_img);
            ((ViewGroup) eventImage.getParent()).setTransitionGroup(false);
            photoCache.put(position, eventImage.getDrawingCache());

            // Configuración de la transicción para el DetailActivity
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(EventsActivity.this,
                    new Pair<View, String>(eventImage, TRANSITION_EVENT + position));

            startActivity(detailIntent, options.toBundle());
        }
    }

    @Override
    public boolean isEventListEmpty() {
        return recyclerAdapter.getEventList().isEmpty();
    }

    @Override
    public boolean onItemClick(AdapterView<?> adapterView, View view, int i, long l,
                               IDrawerItem iDrawerItem) {
        if (iDrawerItem != null) {
            presenter.onCategoryClicked(iDrawerItem.getIdentifier());
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        photoCache = new SparseArray<>(1);
        presenter = new EventsPresenterImpl(this);
        events = new ArrayList<>();
        recyclerAdapter = new EventsAdapter(events);
        recyclerAdapter.setOnItemClickListener(this);

        /*
        * Con esta línea inyectamos las vistas y quedan listas para ser
        * utilizadas. Gracias a ButterKnife ahorramos un montón de líneas
        * repetitivas.
         */
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_menu)
                .actionBarSize().color(Color.WHITE));

        initializeDrawer();
        initializeRecycler();

        presenter.onCreate();
    }

    /**
     * Inicializa el Drawer con todas las opciones y configuraciones.
     */
    private void initializeDrawer() {
        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withHeader(R.layout.header)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_all)
                                .withIdentifier(Category.ALL)
                                .withIcon(GoogleMaterial.Icon.gmd_landscape),
                        new PrimaryDrawerItem().withName(R.string.drawer_litareture)
                                .withIdentifier(Category.LITERATURE)
                                .withIcon(GoogleMaterial.Icon.gmd_local_library),
                        new PrimaryDrawerItem().withName(R.string.drawer_theater_dance)
                                .withIdentifier(Category.THEATER_AND_DANCE)
                                .withIcon(GoogleMaterial.Icon.gmd_local_play),
                        new PrimaryDrawerItem().withName(R.string.drawer_music)
                                .withIdentifier(Category.MUSIC)
                                .withIcon(GoogleMaterial.Icon.gmd_equalizer),
                        new PrimaryDrawerItem().withName(R.string.drawer_cine)
                                .withIdentifier(Category.CINE)
                                .withIcon(GoogleMaterial.Icon.gmd_local_movies),
                        new PrimaryDrawerItem().withName(R.string.drawer_expositions)
                                .withIdentifier(Category.EXPOSITIONS)
                                .withIcon(GoogleMaterial.Icon.gmd_people),
                        new PrimaryDrawerItem().withName(R.string.drawer_more_culture)
                                .withIdentifier(Category.MORE_CULTURE)
                                .withIcon(GoogleMaterial.Icon.gmd_local_offer),
                        new PrimaryDrawerItem().withName(R.string.drawer_holidays)
                                .withIdentifier(Category.HOLIDAYS)
                                .withIcon(GoogleMaterial.Icon.gmd_tag_faces),
                        new PrimaryDrawerItem().withName(R.string.drawer_others)
                                .withIdentifier(Category.OTHERS)
                                .withIcon(GoogleMaterial.Icon.gmd_local_offer)
                )
                .withOnDrawerItemClickListener(this)
                .build();
    }

    /**
     * Realiza todas las configuraciones del RecyclerView para mostrar
     * la lista de eventos.
     */
    private void initializeRecycler() {
        eventRecycler.setLayoutManager(new GridLayoutManager(this, COLUMNS));
        eventRecycler.addItemDecoration(new RecyclerInsetsDecoration(this));
        eventRecycler.setAdapter(recyclerAdapter);
    }
}