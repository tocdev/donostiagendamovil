package com.johanpg.donostiagenda.ui.utils;

import android.transition.Transition;

/**
 * Created by Johan on 5/29/2015.
 */
public class CustomTransitionListener implements Transition.TransitionListener {
    @Override
    public void onTransitionStart(Transition transition) {

    }

    @Override
    public void onTransitionEnd(Transition transition) {

    }

    @Override
    public void onTransitionCancel(Transition transition) {

    }

    @Override
    public void onTransitionPause(Transition transition) {

    }

    @Override
    public void onTransitionResume(Transition transition) {

    }
}
