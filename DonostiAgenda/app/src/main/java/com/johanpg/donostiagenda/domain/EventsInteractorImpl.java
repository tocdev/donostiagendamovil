package com.johanpg.donostiagenda.domain;

import com.johanpg.donostiagenda.commons.BusProvider;
import com.johanpg.donostiagenda.model.entities.Event;
import com.johanpg.donostiagenda.model.rest.RestEventSource;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * <h3>Implementacion de {@link EventsInteractor}</h3>
 * <p/>
 * Esta clase controla las operaciones que vienen solicitadas a través
 * del presentador y cuando estas peticiones están resueltas se lo
 * comunica al presentador.
 *
 * @author Johan
 * @since 5/22/2015.
 */
public class EventsInteractorImpl implements EventsInteractor {

    private RestEventSource restEventSource;

    public static Bus restBus;
    public static Bus uiBus;

    /**
     * Inicializa las fuente de datos (REST) y el bus para comunicarse
     * con este. Además, recibe el bus del presentador.
     *
     * @param bus
     */
    public EventsInteractorImpl(Bus bus) {
        restBus = BusProvider.getRestBusInstance();
        restBus.register(this);
        restEventSource = new RestEventSource(restBus);
        this.uiBus = bus;
    }

    @Override
    public void getEvents() {
        restEventSource.getEvents();
    }

    /**
     * Subscribimos este método para saber cuando están listos los
     * datos desde la fuente.
     *
     * @param events
     */
    @Subscribe
    public void onEvents(ArrayList<Event> events) {
        List<Event> eventsData = new ArrayList();
        for (Event event : events) {
            eventsData.add(event);
        }

        // Comunicamos al presentador que los datos están listos.
        uiBus.post(eventsData);
    }

    /**
     * Subscribimos este método para saber cuando ha habido un error
     * obteniendo los datos.
     *
     * @param error
     */
    @Subscribe
    public void onError(String error) {
        uiBus.post(error);
    }

    @Override
    public void getEventsByCategory(int category) {
        restEventSource.getEventsByCategory(category);
    }
}
