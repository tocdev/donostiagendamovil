package com.johanpg.donostiagenda.commons;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Esta clase consiste en un factoría de objetos de la clase Bus.
 *
 * @author Johan
 * @see <a href="http://square.github.io/otto/">Otto by Square</a>
 * @since 5/23/2015.
 */
public class BusProvider {
    private static final Bus REST_BUS = new Bus(ThreadEnforcer.ANY);
    private static final Bus UI_BUS = new Bus();

    /**
     * Instancia de un Bus para la comunicación con el REST. Este bus
     * es forzado a trabajar en un hilo independiente al principal.
     *
     * @return Bus
     */
    public static Bus getRestBusInstance() {
        return REST_BUS;
    }

    /**
     * Instancia de un bus para la comuncación de eventos relacionados
     * con la interfaz de usuario. Este bus se ejecuta en el hilo principal.
     *
     * @return
     */
    public static Bus getUIBusInstance() {
        return UI_BUS;
    }
}
