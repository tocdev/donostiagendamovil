package com.johanpg.donostiagenda.ui.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.johanpg.donostiagenda.R;
import com.johanpg.donostiagenda.model.entities.Event;
import com.johanpg.donostiagenda.ui.views.views.RecyclerViewClickListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * <h3>Adapter para el RecyclerView de Eventos </h3>
 * <p/>
 * Esta clase adapta, configura y controla los datos que mostrará el
 * RecyclerView de la lista de eventos.
 *
 * @uthor Johan
 * @sine 5/24/2015.
 */
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventsViewHolder> {

    private ArrayList<Event> eventList;
    private Context context;
    private int defaultBackgroundcolor;
    private RecyclerViewClickListener recyclerViewClickListener;
    private static final int SCALE_DELAY = 30;

    public EventsAdapter(ArrayList<Event> eventList) {
        this.eventList = eventList;
    }

    public void setOnItemClickListener(RecyclerViewClickListener recyclerViewClickListener) {
        this.recyclerViewClickListener = recyclerViewClickListener;
    }

    @Override
    public EventsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View rowView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_event, viewGroup, false);

        this.context = viewGroup.getContext();
        defaultBackgroundcolor = context.getResources().getColor(R.color.event_without_palette);

        return new EventsViewHolder(rowView, recyclerViewClickListener);
    }

    @Override
    public void onBindViewHolder(final EventsViewHolder eventsViewHolder, final int position) {
        final Event currentEvent = eventList.get(position);
        eventsViewHolder.eventTitle.setText(currentEvent.getTitle());
        eventsViewHolder.eventDate.setText(currentEvent.getDatetime());
        eventsViewHolder.eventImage.setDrawingCacheEnabled(true);

        Picasso.with(context)
                .load(currentEvent.getImage())
                .fit().centerCrop()
                .into(eventsViewHolder.eventImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        amimateCell(eventsViewHolder);
                    }

                    @Override
                    public void onError() {
                        // TODO: Tratar el error de carga.
                    }
                });
    }

    /**
     * Refresca los datos en la colección de eventos.
     *
     * @param events
     */
    public void refreshEvents(ArrayList<Event> events) {
        int count = eventList.size();
        eventList.removeAll(eventList);
        notifyItemRangeRemoved(0, count);
        eventList.addAll(events);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    /**
     * Devuelve la lista de Eventos actual.
     *
     * @return ArrayList
     */
    public ArrayList<Event> getEventList() {
        return this.eventList;
    }

    /**
     * Establece una animacion para cada una de las celdas.
     *
     * @param eventsViewHolder
     */
    private void amimateCell(EventsViewHolder eventsViewHolder) {
        int cellPosition = eventsViewHolder.getAdapterPosition();

        if (!eventsViewHolder.animated) {
            eventsViewHolder.animated = true;
            eventsViewHolder.eventContainer.setScaleY(0);
            eventsViewHolder.eventContainer.setScaleX(0);
            eventsViewHolder.eventContainer.animate()
                    .scaleY(1).scaleX(1)
                    .setDuration(200)
                    .setStartDelay(SCALE_DELAY * cellPosition)
                    .start();
        }
    }

    /**
     * Clase manejadora de cada una de las celda del RecyclerView.
     */
    protected class EventsViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.item_event_container)
        LinearLayout eventContainer;
        @InjectView(R.id.item_event_text_container)
        RelativeLayout eventTextContainer;
        @InjectView(R.id.item_event_img)
        ImageView eventImage;
        @InjectView(R.id.item_event_title)
        TextView eventTitle;
        @InjectView(R.id.item_event_date)
        TextView eventDate;

        private RecyclerViewClickListener recyclerViewClickListener;
        protected boolean animated = false;

        public EventsViewHolder(View itemView, RecyclerViewClickListener recyclerViewClickListener) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            this.recyclerViewClickListener = recyclerViewClickListener;
        }

        @OnClick(R.id.item_event_img)
        public void onClick(View view) {
            recyclerViewClickListener.onEventClick(view, getAdapterPosition());
        }
    }
}