package com.johanpg.donostiagenda.ui.views.views;

import android.widget.ScrollView;

/**
 * Created by Johan on 5/25/2015.
 */
public interface ScrollViewListener {
    void onScrollChanged(ScrollView scrollView, int x, int y, int oldx, int oldy);
}
