package com.johanpg.donostiagenda.model.rest;

import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.johanpg.donostiagenda.model.entities.Event;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Esta clase implementa lo necesario para realizar las peticiones al
 * API de DonostiAgenda.
 *
 * @author Johan
 * @since 5/23/2015.
 */
public class RestEventSource {

    private static final String API_URL = "http://donostiagenda.hol.es";
    private final Bus bus;
    private DonostiAgendaAPI donostiAgendaAPI;

    /**
     * Inicializa una instancia de {@link DonostiAgendaAPI} y recibe
     * el bus para comunicarse con el interactor.
     *
     * @param bus
     */
    public RestEventSource(Bus bus) {
        this.bus = bus;

        // Creamos una instancia de Gson para decirle al RestAdapter
        // que politica de nombres tiene que aplicar. Los nombres de
        // campo como id_event serán idEvent.
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .setConverter(new GsonConverter(gson))
                .build();

        // Asignamos una instancia de la interfaz DonostiAgendaApi.
        donostiAgendaAPI = restAdapter.create(DonostiAgendaAPI.class);
    }

    /**
     * Ejecuta el método correspondiente para obtener los eventos.
     */
    public void getEvents() {
        donostiAgendaAPI.getEvents(retrofitCallback);
    }

    /**
     * Ejecuta el método correspondiente para obtener los eventos
     * por categoria.
     *
     * @param category Id de la categoria.
     */
    public void getEventsByCategory(int category) {
        donostiAgendaAPI.getEventsByCategory(category, retrofitCallback);
    }

    /**
     * Este método se ejecutará cuando la llamada al API finalze.
     */
    public Callback retrofitCallback = new Callback() {
        @Override
        public void success(Object o, Response response) {
            List<Event> events = (ArrayList<Event>) o;
            bus.post(events);
            Log.v("Retrofit REST", "success");
        }

        @Override
        public void failure(RetrofitError retrofitError) {
            bus.post(retrofitError.getMessage());
            Log.e("Retrofit REST", retrofitError.getMessage());
        }
    };
}
