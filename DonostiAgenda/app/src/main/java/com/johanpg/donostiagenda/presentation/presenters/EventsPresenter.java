package com.johanpg.donostiagenda.presentation.presenters;

import android.view.View;

/**
 * <h3>Presentador de eventos</h3>
 * <p/>
 * Interfaz para conectar la vista de eventos con el modelo de datos.
 * Controla los eventos enrutados por la vista.
 *
 * @author Johan
 * @since 5/22/2015.
 */
public interface EventsPresenter {

    /**
     * Se ejecuta en el onCreate() de la activity y se encarga de
     * obtener los eventos desde el modelo.
     */
    void onCreate();

    /**
     * Abre la vista detalle de un evento al ser pulsado.
     *
     * @param position
     */
    void onEventClicked(View view, int position);

    /**
     * Actualiza los eventos con los elegidos por la categoria.
     *
     * @param category
     */
    void onCategoryClicked(int category);
}
