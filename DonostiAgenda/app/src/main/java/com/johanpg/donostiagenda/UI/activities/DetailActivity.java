package com.johanpg.donostiagenda.ui.activities;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.johanpg.donostiagenda.R;
import com.johanpg.donostiagenda.model.entities.Event;
import com.johanpg.donostiagenda.ui.utils.CustomAnimatorListener;
import com.johanpg.donostiagenda.ui.utils.CustomTransitionListener;
import com.johanpg.donostiagenda.ui.utils.GUIUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class DetailActivity extends Activity {

    @InjectView(R.id.card_view)
    FrameLayout contentCard;
    @InjectView(R.id.activity_detail_share)
    View shareButton;
    @InjectView(R.id.activity_detail_main_container)
    View mainContaienr;
    @InjectView(R.id.activity_detail_titles_container)
    View titlesContainer;
    @InjectView(R.id.activity_detail_toolbar)
    Toolbar toolbar;
    @InjectView(R.id.activity_detail_event_info)
    LinearLayout eventInfoLayout;
    @InjectView(R.id.activity_detail_content)
    TextView contentTextView;
    @InjectView(R.id.activity_detail_summary_title)
    TextView summaryTitle;
    @InjectView(R.id.activity_detail_title)
    TextView titleTextView;
    @InjectView(R.id.activity_detail_subtitle)
    TextView subtitleTextView;
    @InjectView(R.id.activity_detail_date_title)
    TextView dateTitleTextView;
    @InjectView(R.id.activity_detail_date_value)
    TextView dateValueTextView;

    private AsyncTask<Bitmap, Void, Palette> palette;
    private Event selectedEvent;
    private static final String EVENT_SHARE_HASHTAG = " #DonostiAgenda";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.inject(this);

        // Obtenemos el evento seleccionado.
        final int position = getIntent().getIntExtra(EventsActivity.EXTRA_POSITION, 0);
        selectedEvent = (Event) getIntent().getSerializableExtra(EventsActivity.EXTRA_EVENT);

        // Obtenemos la cache de la imagen del evento.
        Bitmap eventImageBitmap = EventsActivity.photoCache.get(position);
        ImageView toolbarEventImage = (ImageView) findViewById(R.id.activity_detail_image);
        toolbarEventImage.setImageBitmap(eventImageBitmap);

        // Share button
        shareButton.setScaleX(0);
        shareButton.setScaleY(0);

        // Configuramos el punto de partida para las animaciones.
        GUIUtils.configureHideYView(contentCard);
        GUIUtils.configureHideYView(eventInfoLayout);
        GUIUtils.configureHideYView(mainContaienr);

        toolbar.setBackground(new BitmapDrawable(getResources(), eventImageBitmap));
        toolbar.setTransitionName(EventsActivity.TRANSITION_EVENT + position);

        // Add a listener to get noticed when the transition ends to animate the fab button
        getWindow().getSharedElementEnterTransition().addListener(sharedTransitionListener);

        // Generamos la paleta de colores personalizados.
        palette = Palette.from(eventImageBitmap).generate(paletteListener);
    }

    /**
     * Comparte datos del evento a las aplicaciones del sistema.
     */
    @OnClick(R.id.activity_detail_share)
    public void onShareClick() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        shareIntent.putExtra(Intent.EXTRA_TEXT, (selectedEvent.getTitle() + ", "
                + selectedEvent.getDatetime() + EVENT_SHARE_HASHTAG));
        shareIntent.setType("text/plain");
        startActivity(shareIntent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        ViewPropertyAnimator hideTitleAnimator = GUIUtils.hideViewByScaleXY(shareButton);

        titlesContainer.startAnimation(AnimationUtils.loadAnimation(DetailActivity.this, R.anim.alpha_off));
        titlesContainer.setVisibility(View.INVISIBLE);

        GUIUtils.hideViewByScaleY(eventInfoLayout);

        hideTitleAnimator.setListener(new CustomAnimatorListener() {

            @Override
            public void onAnimationEnd(Animator animation) {

                ViewPropertyAnimator hideFabAnimator = GUIUtils.hideViewByScaleY(mainContaienr);
                hideFabAnimator.setListener(new CustomAnimatorListener() {

                    @Override
                    public void onAnimationEnd(Animator animation) {

                        super.onAnimationEnd(animation);
                        coolBack();
                    }
                });
            }
        });

        hideTitleAnimator.start();
    }

    private void coolBack() {
        try {
            super.onBackPressed();
        } catch (NullPointerException e) {
            // TODO: workaround
        }
    }

    /**
     * I use a listener to get notified when the enter transition ends, and with that notifications
     * build my own coreography built with the elements of the UI
     * <p/>
     * Animations order:
     * <p/>
     * 1. The image is animated automatically by the SharedElementTransition
     * 2. The layout that contains the titles
     * 3. An alpha transition to show the text of the titles
     * 3. A scale animation to show the book info
     */
    private CustomTransitionListener sharedTransitionListener = new CustomTransitionListener() {

        @Override
        public void onTransitionEnd(Transition transition) {

            super.onTransitionEnd(transition);

            ViewPropertyAnimator showTitleAnimator = GUIUtils.showViewByScale(mainContaienr);
            showTitleAnimator.setListener(new CustomAnimatorListener() {

                @Override
                public void onAnimationEnd(Animator animation) {

                    super.onAnimationEnd(animation);
                    titlesContainer.startAnimation(AnimationUtils.loadAnimation(DetailActivity.this,
                            R.anim.alpha_on));
                    titlesContainer.setVisibility(View.VISIBLE);

                    GUIUtils.showViewByScale(shareButton).start();
                    GUIUtils.showViewByScale(eventInfoLayout).start();
                }
            });

            showTitleAnimator.start();
        }
    };

    /**
     * Listener para la paleta de colores.
     * <p/>
     * Una vez los colores han sido extraidos correctamente,
     * establecemos los colores en las vistas.
     */
    private Palette.PaletteAsyncListener paletteListener = new Palette.PaletteAsyncListener() {

        @Override
        public void onGenerated(Palette palette) {
            Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();

            // Establecemos los colores obtenideos de la Paleta.
            if (vibrantSwatch != null) {
                getWindow().setStatusBarColor(vibrantSwatch.getRgb());
                getWindow().setNavigationBarColor(vibrantSwatch.getRgb());
                mainContaienr.setBackgroundColor(vibrantSwatch.getRgb());
                subtitleTextView.setTextColor(vibrantSwatch.getTitleTextColor());
                summaryTitle.setTextColor(vibrantSwatch.getRgb());
                titleTextView.setTextColor(vibrantSwatch.getTitleTextColor());
                subtitleTextView.setTextColor(vibrantSwatch.getTitleTextColor());
                dateTitleTextView.setTextColor(vibrantSwatch.getRgb());
            }

            contentTextView.setText(selectedEvent.getSummary());
            titleTextView.setText(selectedEvent.getTitle());
            subtitleTextView.setText(selectedEvent.getDatetime());
            dateValueTextView.setText(selectedEvent.getDatetime());
        }
    };
}