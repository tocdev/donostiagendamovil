package com.johanpg.donostiagenda.model.entities;

/**
 * <h3>Entidad Categoria</h3>
 * <p/>
 * Esta clase contiene información de las categorias.
 *
 * @author Johan
 * @since 6/1/2015.
 */
public class Category {

    public static final int ALL = 0;
    public static final int THEATER_AND_DANCE = 1;
    public static final int MUSIC = 2;
    public static final int CINE = 3;
    public static final int EXPOSITIONS = 4;
    public static final int LITERATURE = 5;
    public static final int MORE_CULTURE = 6;
    public static final int HOLIDAYS = 7;
    public static final int OTHERS = 8;
}
