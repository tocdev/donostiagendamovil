package com.johanpg.donostiagenda.ui.views.views;

import android.view.View;

/**
 * <h3>Listener para el RecyclerView</h3>
 * <p/>
 * Esta interfaz permite comunica Adapter con la Activity
 *
 * @author Johan
 * @since 5/24/2015.
 */
public interface RecyclerViewClickListener {

    /**
     * Se ejecuta en el OnClick de la vista seleccionada.
     *
     * @param view
     * @param position
     */
    void onEventClick(View view, int position);
}
