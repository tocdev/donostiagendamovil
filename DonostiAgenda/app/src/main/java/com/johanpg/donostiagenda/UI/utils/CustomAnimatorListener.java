package com.johanpg.donostiagenda.ui.utils;

import android.animation.Animator;

/**
 * Created by Johan on 5/29/2015.
 */
public class CustomAnimatorListener implements Animator.AnimatorListener {
    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {

    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}
