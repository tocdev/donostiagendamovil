package com.johanpg.donostiagenda.domain;

/**
 * <h3>Interactor de eventos</h3>
 * <p/>
 * Esta interfaz controla los casos de uso posibles con los datos
 * de los eventos.
 *
 * @author Johan
 * @since 5/22/2015.
 */
public interface EventsInteractor {

    /**
     * Obtiene los eventos desde la fuente de datos.
     */
    void getEvents();

    /**
     * Obtiene los eventos desde la fuentes de datos filtrando por la
     * categoria pasada.
     *
     * @param category
     */
    void getEventsByCategory(int category);
}
