package com.johanpg.donostiagenda.model.rest;

import com.johanpg.donostiagenda.model.entities.Event;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * <h3>API de DonostiAgenda</h3>
 * <p/>
 * Esta interfaz permite hacer un consumo de nuestro REST API.
 *
 * @author Johan
 * @see <a href="http://square.github.io/retrofit/">Retrofit by Square</a>
 * @since 5/23/2015.
 */
public interface DonostiAgendaAPI {

    /**
     * Obtiene todos los eventos.
     *
     * @param events
     */
    @GET("/events_api/events")
    void getEvents(Callback<List<Event>> events);

    /**
     * Obtiene los eventos por categoria.
     *
     * @param category
     * @param events
     */
    @GET("/events_api/events/id_category/{category}")
    void getEventsByCategory(@Path("category") int category, Callback<List<Event>> events);
}
