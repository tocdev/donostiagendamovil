package com.johanpg.donostiagenda;

import com.johanpg.donostiagenda.model.entities.Event;
import com.johanpg.donostiagenda.model.rest.DonostiAgendaAPI;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * <h3>DummyCaller</h3>
 * <p/>
 * Esta clase ejecuta las llamadas al API de DonostiAgenda para ser
 * testeado.
 *
 * @author Johan
 * @since 6/3/2015.
 */
public class DummyCaller implements Callback {

    private final DonostiAgendaAPI donostiAgendaAPI;

    private ArrayList<Event> events = new ArrayList<>();

    /**
     * Inicializa los componentes para hacer las llamadas al API.
     *
     * @param donostiAgendaAPI
     */
    public DummyCaller(DonostiAgendaAPI donostiAgendaAPI) {
        this.donostiAgendaAPI = donostiAgendaAPI;
    }

    /**
     * Obtiene los eventos de DonostiAgenda.
     */
    public void getEventsAPI() {
        donostiAgendaAPI.getEvents(this);
    }

    /**
     * Obtiene los eventos de DonostiAgenda por categoria.
     *
     * @param id
     */
    public void getEventsById(int id) {
        donostiAgendaAPI.getEventsByCategory(id, this);
    }

    /**
     * Devuelve los eventos obtenidos.
     *
     * @return
     */
    public ArrayList<Event> getEvents() {
        return events;
    }

    @Override
    public void success(Object o, Response response) {
        events = (ArrayList<Event>) o;
    }

    @Override
    public void failure(RetrofitError retrofitError) {

    }
}
