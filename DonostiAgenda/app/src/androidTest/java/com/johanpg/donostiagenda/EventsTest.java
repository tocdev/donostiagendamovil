package com.johanpg.donostiagenda;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.johanpg.donostiagenda.model.entities.Category;
import com.johanpg.donostiagenda.model.rest.DonostiAgendaAPI;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * <h3>Unit Test de eventos</h3>
 * <p/>
 * Esta clase implementa test unitarios para comprobar los datos de
 * los eventos obtenidos por la aplicación.
 *
 * @auhor Johan
 * @since 6/2/2015.
 */

@RunWith(MockitoJUnitRunner.class)
public class EventsTest {

    private DummyCaller dummyCaller;
    @Mock
    private DonostiAgendaAPI donostiAgendaAPI;
    @Captor
    private ArgumentCaptor<Callback> callbackCaptor;
    private static final String API_URL = "http://donostiagenda.hol.es";

    /**
     * Inicializa los componentes necesarios para ejecutar las pruebas.
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .setConverter(new GsonConverter(gson))
                .build();

        // Asignamos una instancia de la interfaz DonostiAgendaApi.
        donostiAgendaAPI = restAdapter.create(DonostiAgendaAPI.class);
        dummyCaller = new DummyCaller(donostiAgendaAPI);
    }

    /**
     * Comprueba si los datos de los eventos son obtenidos correctamente.
     */
    @Test
    public void testEvents() {
        dummyCaller.getEventsAPI();

        /*
         * Gracias a Mockito, podemos hacer pruebas con tareas que
         * se ejecutan en asincrono.
         */
        verify(donostiAgendaAPI, times(1)).getEvents(callbackCaptor.capture());
        assertNull(dummyCaller.getEvents());
    }

    /**
     * Comprueba si los datos de los eventos filtrados por cateoria
     * son obtenidos correctamente.
     */
    @Test
    public void testEventsByCategory() {
        dummyCaller.getEventsById(Category.HOLIDAYS);

        verify(donostiAgendaAPI, times(1)).getEventsByCategory(Category.HOLIDAYS,
                callbackCaptor.capture());
        assertNull(dummyCaller.getEvents());
    }
}
